import React from 'react';
import logo from './logo.svg';
import './App.css';
import List from './Component/list';
import Add from "./Component/add";
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Route exact path="/" component={List} />
        <Route path="/add" component={Add} />
      </BrowserRouter>
    </div>
  );
}
export default App;
