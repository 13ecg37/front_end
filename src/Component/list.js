import React, { Component, Fragment } from 'react'
import "./list.css"
import { instance } from './constant';
import swal from "sweetalert";
import { Button } from 'reactstrap';
import { faPencilAlt, faTrashAlt, faCheckCircle, faTimes, faAngleLeft, faAngleRight, faAngleDoubleLeft, faAngleDoubleRight, } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';
import JwPagination from 'jw-react-pagination';
import ReactTooltip from "react-tooltip";
const customLabels = {
  first: <FontAwesomeIcon icon={faAngleDoubleLeft} />,
  last: <FontAwesomeIcon icon={faAngleDoubleRight} />,
  previous: <FontAwesomeIcon icon={faAngleLeft} />,
  next: <FontAwesomeIcon icon={faAngleRight} />
}
class List extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userdata: [],
      pageOfItems: [],
      searchInput: ""

    }
  }
  componentDidMount() {
    instance.get("api/").then(res => {
      console.log(res.data, "Get Data")
      this.setState({ userdata: res.data })
    }).catch(error => {
      console.log(error, "Error");
    })
  }
  onChangePage = (pageOfItems) => {
    this.setState({ pageOfItems }); //pagination
  }
  handleChange = event => {
    console.log(event.target.value)
    this.setState({ searchInput: event.target.value }, () => {
      this.globalSearch();
    });
  };
  globalSearch = () => {
    let { searchInput, userdata } = this.state;
    let filteredData = userdata.filter(value => {
      return (
        value.name.toLowerCase().includes(searchInput.toLowerCase()) ||
        value.Shopname.toLowerCase().includes(searchInput.toLowerCase()) ||
        value.Status
          .toString()
          .toLowerCase()
          .includes(searchInput.toLowerCase())
      );
    });
    this.setState({ pageOfItems: filteredData });
  };
  delete = (data) => {
    console.log(data, "data")
    instance.delete(`api/${data.id}/`).then(res => {
      console.log(res, "respsine")
      swal({
        title: "Deleted Sucessfully",
        icon: "success",
        button: "Aww yiss!",
      });
    }).catch(error => {
      console.log(error, "Error");
    })
    this.props.history.push("/")
    this.forceUpdate()
  }
  render() {
    return (
      <Fragment>
        <div className="list_comp">
          <div className="section_one">
          <h2>Demo Project</h2>
            <div className="inputbox">

              <input type="search" className="input_typebox"
                value={this.state.searchInput || ""}
                onChange={this.handleChange}
                name="Search box"
                placeholder="Search"></input>
              <Link to="/add" > <Button color="success">Add</Button> </Link>
            </div>
            <br />
            <table className="tabele">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Shopname</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {this.state.pageOfItems.map(data => {
                  return (<tr key={data.id}>
                    <td>{data.name == null ? "hey" : data.name}</td>
                    <td>{data.Shopname == null ? "hey" : data.Shopname}</td>
                    <td>{data.Status == null ? "hey" : data.Status}</td>
                    <td> <div className="buttonclass"><Link to={{
                      pathname: "/add",
                      query: data
                    }}>
                      <ReactTooltip className="customeTheme" id='edit_button' type='info' effect='solid'>
                        <span>Edit</span>
                      </ReactTooltip>
                      <ReactTooltip className="customeTheme" id='delete_button' type='info' effect='solid'>
                        <span>Delete</span>
                      </ReactTooltip>
                      <button data-tip data-for="edit_button"><FontAwesomeIcon icon={faPencilAlt} /></button>
                    </Link>
                      <button data-tip data-for="delete_button" onClick={() => this.delete(data)}><FontAwesomeIcon icon={faTrashAlt} /></button>
                    </div>   </td>
                  </tr>)
                })
                }
              </tbody>
            </table>
            <div className="Pagination">
              <JwPagination
                pageSize={7}
                items={this.state.userdata}
                onChangePage={this.onChangePage}
                labels={customLabels}
              />
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}
export default List
