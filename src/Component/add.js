import React, { Component } from 'react'
import { instance } from './constant';
import swal from "sweetalert";
import { Button } from 'reactstrap';
class Add extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            shoepName: "",
            messages: "",
            addOrEdit: 'add',
            editId: "",
        }
    }
    componentDidMount() {
        console.log(this.props.location.query, "Qurerry data")
        if (this.props.location.query) {
            this.setState({
                name: this.props.location.query.name,
                shoepName: this.props.location.query.Shopname,
                messages: this.props.location.query.Status,
                addOrEdit: "edit",
                editId: this.props.location.query.id,
            })
        }
    }
    submit = (event) => {
        event.preventDefault();
        if (this.state.addOrEdit === 'add') {
            const body = {
                name: this.state.name,
                Shopname: this.state.shoepName,
                Status: this.state.messages
            }
            const bankEndData = JSON.stringify(body)
            console.log(this, "Body data")
            instance.post("api/", body).then(res => {
                console.log(res, "respsine")
            }).catch(error => { console.log(error, "Error") })
            this.props.history.push("/")
            this.forceUpdate()
        }
        else if (this.state.addOrEdit === 'edit') {
            const body = {
                name: this.state.name,
                Shopname: this.state.shoepName,
                Status: this.state.messages
            }
            const bankEndData = JSON.stringify(body)
            console.log(body, "Body data")
            instance.put(`api/${this.state.editId}/`, body).then(res => {
                console.log(res, "respsine")
            }).catch(error => {
                console.log(error, "Error");
            })
            this.props.history.push("/")
            this.forceUpdate()
        }
    }
    shopchange = (e) => {
        this.setState({ shoepName: e.target.value })
    }
    messagesChanges = (e) => {
        this.setState({ messages: e.target.value })
    }
    handleinputChange = (event) => {
        this.setState({
            name: event.target.value
        })
    }
    render() {
        return (
            <div className="section_one">

                <form onSubmit={this.submit}>
                    <label><b>NAME:</b></label> <br></br>
                    <input type="text" name="username" className="input" value={this.state.name} onChange={this.handleinputChange} required></input>
                    <br></br>
                    <label><b>SHOP NAME</b></label><br></br>
                    <input type="text" name="shopname" className="input" value={this.state.shoepName} onChange={this.shopchange} required></input>
                    <br></br>
                    <label><b>STATUS</b></label> <br></br>
                    <textarea name="message" value={this.state.messages} onChange={this.messagesChanges} required>
                    </textarea>
                    <br></br>
                    <Button color="success">Submit</Button>
                    <br></br>
                </form>
            </div>
        )
    }
}
export default Add
